import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import RootNavigation from "./src/navigation/RootNavigation";
import {
    View,
    Text,
    StatusBar,
    Platform
} from 'react-native';

const App: React.FC = () => {
    return (
        <>
            {
                Platform.OS === "ios" && <StatusBar barStyle="dark-content"/>
            }
            <NavigationContainer>
                <RootNavigation/>
            </NavigationContainer>
        </>
    );
};


export default App;
