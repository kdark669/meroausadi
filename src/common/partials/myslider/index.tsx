import React from 'react';
import {SafeAreaView, ScrollView, View} from "react-native";
import layout from "../../../assets/styles/layout";

interface MySliderProps {
    children: any
}

const MySlider: React.FC<MySliderProps> = (props) => {
    const {
        children
    } = props
    return (
        <ScrollView
            style={{paddingVertical:3}}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            scrollEventThrottle={16}
            pagingEnabled={true}
            alwaysBounceHorizontal={true}
            automaticallyAdjustContentInsets={true}
        >
            {children}
        </ScrollView>
    );
};
export default MySlider;
