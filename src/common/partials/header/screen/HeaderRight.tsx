import React from 'react';
import {StyleSheet, View, TouchableOpacity,Image} from "react-native";
import { useNavigation } from '@react-navigation/native';
import {HeaderRightPropsInterface, HeaderRightStyleInterface} from "../interface/HeaderInterface";
import colors from "../../../../assets/styles/colors";
import IconButton from "../../iconbutton";
import layout from "../../../../assets/styles/layout";

const HeaderRight: React.FC<HeaderRightPropsInterface> = () => {
    const navigation = useNavigation();
    return (
        <View style={[styles.header, layout.displayRow, layout.alignCenter]}>
            <View style={styles.headerIcon}>
                <IconButton
                    iconName={"shopping-cart"}
                    iconColor={colors.dark}
                    iconSize={25}
                    onPressAction={() =>navigation.navigate('Cart')}
                />
            </View>
            <TouchableOpacity
                style={styles.headerIcon}
                onPress={() => navigation.navigate('Home')}
            >
                <View style={styles.imageContainer}>
                    <Image
                        style={layout.circularImg}
                        resizeMethod={'auto'}
                        resizeMode={'cover'}
                        source={{
                            uri: 'https://cdn.pixabay.com/photo/2020/08/29/08/33/woman-5526487_1280.jpg',
                        }}
                    />
                </View>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create<HeaderRightStyleInterface>({
    header: {
        paddingHorizontal: 20
    },
    headerIcon: {
        paddingHorizontal: 10
    },
    imageContainer:{
        width:30,
        height:30
    }
})

export default HeaderRight;
