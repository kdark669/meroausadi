import React from 'react';
import {
    View,
    Text, StyleSheet, ViewStyle, TextStyle, TouchableOpacity
} from 'react-native'
import colors from "../../../assets/styles/colors";
import typography from "../../../assets/styles/typography";
import MyIcon from "../myicon";
import layout from "../../../assets/styles/layout";

interface MyButtonPropInterface {
    buttonName: string
    buttonColor?: string
    buttonIcon: string
    onPressAction?: () => void
}

interface MyButtonStyleInterface {
    button: ViewStyle
    buttonIconStyle: ViewStyle
    buttonText: TextStyle
}

const MyButton: React.FC<MyButtonPropInterface> = (props) => {
    const {
        buttonName,
        buttonColor,
        buttonIcon,
        onPressAction
    } = props
    return (
        <TouchableOpacity
            onPress={() => onPressAction ? onPressAction : console.log('I am button press ')}
            style={[
                styles.button,
                layout.displayRow,
                layout.alignCenter,
                layout.displayCenter,
                {
                    backgroundColor: buttonColor ? buttonColor : colors.dark
                }
            ]}>
            <View style={styles.buttonIconStyle}>
                <MyIcon
                    iconName={buttonIcon}
                    iconColor={colors.light}
                    iconSize={25}
                />
            </View>
            <Text style={[
                typography.subTitle,
                styles.buttonText
            ]}>
                {buttonName}
            </Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create<MyButtonStyleInterface>({
    button: {
        padding: 10,
        borderRadius: 10
    },
    buttonIconStyle: {
        paddingRight: 15
    },
    buttonText: {
        color: colors.light,
        textAlign: "center"
    }
})
export default MyButton;
