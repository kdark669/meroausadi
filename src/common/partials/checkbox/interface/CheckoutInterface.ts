import {TextStyle, ViewStyle} from "react-native";

export interface CheckboxPropsInterface {
    selected: boolean
    checkBoxName?: string | null
    onPress: () => void
}
export interface  CheckboxStylesInterface{
    checkBox:ViewStyle,
    textStyle:TextStyle
}
