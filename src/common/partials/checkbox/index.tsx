import React from 'react';
import {StyleSheet,TouchableOpacity, Text, View} from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons';
Icon.loadFont()
import {CheckboxPropsInterface, CheckboxStylesInterface} from "./interface/CheckoutInterface";
import colors from "../../../assets/styles/colors";
import layout from "../../../assets/styles/layout";

const CheckBox:React.FC<CheckboxPropsInterface> = (props) => {
    const {
        selected,
        checkBoxName,
        onPress
    } = props
    return (
        <TouchableOpacity
            style={[styles.checkBox, layout.displayRow,layout.alignCenter]}
            onPress={() => onPress() } >
            <Icon
                size={25}
                color={colors.dark}
                name={  selected ? 'check-box' : 'check-box-outline-blank'}
            />
            <Text style={[styles.textStyle]}> { checkBoxName } </Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create<CheckboxStylesInterface>({
    checkBox:{

    },
    textStyle:{
        padding: 10,
        fontSize:15
    }
})

export default CheckBox;
