import React, {useState} from 'react';
import {Platform, StyleSheet, Text, TextInput, View} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import vars from "../../../assets/styles/vars";
import colors from "../../../assets/styles/colors";
import layout from "../../../assets/styles/layout";
import KeyboardDismiss from "../../../hoc/KeyboardDismiss";

const Search:React.FC = () => {
    const [keyword, setKeyword] = useState<string>('')
    const onChange = (text:string) => {
        setKeyword(text)
    }
    return (
        <KeyboardDismiss>
            <View style={[styles.inputContainer, layout.displayRow, layout.alignCenter]}>
                <Icon name='search'
                      size={15}
                      color={colors.dark}
                />
                <TextInput
                    style={[styles.inputBox]}
                    underlineColorAndroid="transparent"
                    onChangeText={text => onChange(text)}
                    value={keyword}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    placeholder={"Search ..."}
                />
            </View>
        </KeyboardDismiss>
    );
};
const styles = StyleSheet.create({
    inputContainer: {
        width: "100%",
        backgroundColor: colors.light,
        borderRadius: vars.inputRadius,
        textAlign: "center",
        paddingLeft:7
    },
    inputBox: {
        marginLeft: 10,
        width: "100%",
        padding: Platform.OS === "ios" ? 12 : 10,
    }
})
export default Search;
