import React from 'react';
import {StyleSheet, Text, TextStyle, TouchableHighlight, View, Alert,ViewStyle} from "react-native";
import layout from "../../../assets/styles/layout";
import {heightPercentageToDP as hp} from "react-native-responsive-screen";
import colors from "../../../assets/styles/colors";
import Icon from "react-native-vector-icons/FontAwesome";

interface MyFooterPropsInterface {
    buttonName: string
    buttonIcon?:string
    buttonAction: () => void
}
interface MyFooterStyleInterface {
    myFooter:ViewStyle
    myFooterButton:ViewStyle
    buttonContainer:ViewStyle
    textStyle:TextStyle
}

const MyFooter:React.FC<MyFooterPropsInterface> = (props) => {
    const {
        buttonIcon,
        buttonName,
        buttonAction
    } = props
    return (
        <View
            style={[
                styles.myFooter,
                layout.displayRow,
                layout.displayBetween,
                layout.alignCenter
            ]}
        >
            <TouchableHighlight
                style={{...styles.myFooterButton,backgroundColor: colors.primary}}
                onPress={() => buttonAction()}
            >
                <View style={styles.buttonContainer}>
                    {
                        buttonIcon && <Icon
                            name={buttonIcon}
                            color={colors.light}
                            size={25}
                        />
                    }
                    <Text style={styles.textStyle}>{buttonName}</Text>
                </View>
            </TouchableHighlight>
        </View>
    );
};
const styles = StyleSheet.create<MyFooterStyleInterface>({
    myFooter:{
        width:"100%",
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    myFooterButton:{
        padding: 18,
        flex:1,
    },
    buttonContainer:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-evenly"
    },
    textStyle: {
        color: colors.light,
        fontSize:20,
        textTransform:"capitalize",
        fontWeight: "bold",
        textAlign: "center"
    },
})
export default MyFooter;
