import { ViewStyle} from "react-native";

export interface MedItemPropsInterface {
    item: any
}

export interface MedItemStyleInterface {
    medItem: ViewStyle,
    medItemImage: any,
    medItemDetail:ViewStyle
}

export interface MedDetailPropsInterface{

}
export interface MedDetailStyleInterface{
    medDetail: ViewStyle
    concentration: ViewStyle
    concentrationActive: ViewStyle
    medTagItem: ViewStyle
    medDetailDescription: ViewStyle
    activeText: ViewStyle
}
