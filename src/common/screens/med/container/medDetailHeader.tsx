import React from 'react';
import {Image, Platform, StyleSheet, Text, View} from "react-native";
import {
    MedDetailHeaderPropsInterface,
    MedDetailHeaderStylesInterface
} from "../../../../modules/customer/screens/home/interface/homeInterface";
import colors from "../../../../assets/styles/colors";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import layout from "../../../../assets/styles/layout";

const MedDetailHeader:React.FC<MedDetailHeaderPropsInterface> = () => {
    return (
        <View style={styles.homeHeadContainer}>
            <View style={styles.homeHeaderSpacing}>
                <View style={styles.imageContainer}>
                    <Image
                        style={layout.rounderImg}
                        resizeMethod={'auto'}
                        resizeMode={'contain'}
                        source={{
                            uri: 'https://cdn.pixabay.com/photo/2012/04/10/17/40/vitamins-26622_1280.png',
                        }}
                    />
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create<MedDetailHeaderStylesInterface>({
    homeHeadContainer:{
        backgroundColor: colors.primary,
        height:hp('30%')
    },
    homeHeaderSpacing:{
        marginTop:Platform.OS === "ios" ? hp("10%") :hp("8%"),
        paddingHorizontal:20
    },
    imageContainer:{
        width:Platform.OS === 'ios' ? wp('50%') : wp('55%'),
        paddingHorizontal: 10,
        marginBottom:20,
        marginHorizontal:wp("20%")
    }
})

export default MedDetailHeader;
