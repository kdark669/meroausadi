import React, {useState} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, View, Text, Alert, TouchableOpacity,} from "react-native";
import {heightPercentageToDP} from "react-native-responsive-screen";
import { useNavigation } from '@react-navigation/native';
import {MedDetailPropsInterface, MedDetailStyleInterface} from "../interface/medInterface";

import layout from "../../../../assets/styles/layout";
import typography from "../../../../assets/styles/typography";

import MedDetailHeader from "../container/medDetailHeader";
import QuantitySetter from "../../quantitySetter";
import Card from "../../../partials/card";
import MyFooter from "../../myFooter";
import colors from "../../../../assets/styles/colors";
import vars from "../../../../assets/styles/vars";

const MedDetail: React.FC<MedDetailPropsInterface> = (props) => {
    const {} = props
    const navigation = useNavigation()
    const [concentration, setConcentration] = useState<number>(500)
    const [quantity, setQuantity] = useState<number>(1)

    const setNewConcentration:Function = (concen:number):void => {
        setConcentration(concen)
    }

    const addToCart = () => {
        console.log("quantity" + quantity)
        console.log("concentration" + concentration)
        Alert.alert(
            'Cart',
            'Paracetamol  added to cart',
            [
                {text: "Ok", onPress: () => navigation.navigate('Cart') },
            ]
        )
        // navigation.navigate('Cart')
    }

    return (
        <>
            <View>
                <MedDetailHeader/>
            </View>
            <SafeAreaView style={[layout.safeArea, styles.medDetail]}>
                <ScrollView
                    style={[layout.containerPadding]}
                >
                    <View style={[layout.displayRow, layout.displayBetween, layout.alignCenter]}>
                        <Text style={typography.title}> Paracetamol </Text>
                        <Text> <Text style={typography.costSubTitle}> Rs 30</Text>/Tabs
                        </Text>
                    </View>
                    <View style={layout.safeAreaContent}>
                        <Text style={typography.subTitle}> Concentration </Text>
                        <View
                            style={[layout.displayRow, layout.displayBetween, layout.alignCenter, styles.concentration]}>

                            <TouchableOpacity style={ concentration === 500 && styles.concentrationActive } onPress={() => setNewConcentration(500)}>
                                <Text style={[typography.subTitle, concentration === 500 && styles.activeText]}> 500mg </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={ concentration === 1000 && styles.concentrationActive } onPress={() => setNewConcentration(1000)}>
                                <Text style={[typography.subTitle, concentration === 1000 && styles.activeText]}> 1000mg </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={ concentration === 1500 && styles.concentrationActive } onPress={() => setNewConcentration(1500)}>
                                <Text style={[typography.subTitle, concentration === 1500 && styles.activeText]}> 1500mg </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={layout.safeAreaContent}>
                        <Text style={typography.subTitle}> Quantity </Text>
                        <QuantitySetter
                            isCost={true}
                            quantity={quantity}
                            setQuantity={setQuantity}
                        />
                    </View>
                    <View style={layout.safeAreaContent}>
                        <Text style={typography.subTitle}> Tags </Text>
                        <View style={[layout.safeAreaContentPadding, layout.displayRow, layout.displayCenter]}>
                            <View style={styles.medTagItem}>
                                <Card>
                                    <Text style={typography.subTitle}> Normal</Text>
                                </Card>
                            </View>
                        </View>
                    </View>
                    <View style={[layout.safeAreaContent, styles.medDetailDescription]}>
                        <Text style={typography.subTitle}> Description </Text>
                        <View style={layout.containerPadding}>
                            <Text>
                                Paracetamol (acetaminophen) is a pain reliever and a fever reducer.
                                The exact mechanism of action of is not known.

                                Paracetamol is used to treat many conditions such as headache,
                                muscle aches, arthritis, backache, toothaches, colds, and fevers.
                                It relieves pain in mild arthritis but has no effect on the underlying
                                inflammation and swelling of the joint.

                                Paracetamol may also be used for other purposes not listed in this medication guide.
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
            <MyFooter
                buttonName={"Add To Cart"}
                buttonAction={() => Alert.alert(
                    'Cart',
                    'Are you sure you want to add paracetamol to cart',
                    [
                        {text: "Yes", onPress: () =>addToCart() },
                        {text: "Cancel" }
                    ]
                )}

            />
        </>
    )
        ;
};
const styles = StyleSheet.create<MedDetailStyleInterface>({
    medDetail: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    },
    concentration: {
        padding: 10
    },
    concentrationActive:{
        backgroundColor: colors.active,
        borderRadius:vars.buttonRadius,
        padding:5
    },
    activeText:{
        color: colors.light
    },
    medTagItem: {
        paddingHorizontal: 10
    },
    medDetailDescription: {
        flex: 1,
        paddingBottom:heightPercentageToDP('10%')

    }
})
export default MedDetail;
