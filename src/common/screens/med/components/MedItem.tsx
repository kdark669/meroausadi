import React from 'react';
import { useNavigation } from '@react-navigation/native';
import {heightPercentageToDP as hp} from "react-native-responsive-screen";
import {MedItemPropsInterface, MedItemStyleInterface} from "../interface/medInterface";
import {Image, Platform, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import typography from "../../../../assets/styles/typography";
import layout from "../../../../assets/styles/layout";
import colors from "../../../../assets/styles/colors";

const MedItem:React.FC<MedItemPropsInterface> = (props) => {
    const {
        item
    } = props
    const navigation = useNavigation();
    return (
        <TouchableOpacity
            style={styles.medItem}
            onPress={() =>navigation.navigate('MedDetail')}
        >
            <View style={[styles.medItemImage]}>
                <Image
                    style={layout.rounderImg}
                    resizeMethod={'auto'}
                    resizeMode={'contain'}
                    source={{
                        uri: item ? item.image : 'https://cdn.pixabay.com/photo/2012/04/10/17/40/vitamins-26622_1280.png',
                    }}
                />
            </View>
            <View style={styles.medItemDetail}>
                <Text style={typography.subTitle}>{ item ? item.name : "Augmentin"} </Text>
                <Text style={typography.costSubTitle}>Rs { item ? item.price : 40.00}</Text>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create<MedItemStyleInterface>({
    medItem: {
        flex:1,
        marginHorizontal:10,
        // height:Platform.OS === "ios" ? hp("30%") : hp("35%"),
    },
    medItemImage: {
        padding:10,
        height:Platform.OS === "ios" ? hp("21%") : hp("23%"),
        borderRadius:10,
        backgroundColor:colors.light,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        aspectRatio:1,
    },
    medItemDetail:{
        padding:10,
        textAlign:"center"
    }
})

export default MedItem;
