import React, {useState} from 'react';
import layout from "../../../assets/styles/layout";
import {Alert, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import vars from "../../../assets/styles/vars";
import colors from "../../../assets/styles/colors";
import typography from "../../../assets/styles/typography";
import {QuantitySetterInterfaces} from "./interface/QuantitySetterInterface";

interface QuantitySetterProps {
    isCost: boolean
    quantity: number
    setQuantity: Function
}

const QuantitySetter: React.FC<QuantitySetterProps> = (props) => {
    const { isCost, quantity, setQuantity } = props
    const increaseQuantity = () => {
        setQuantity(quantity + 1)
    }
    const decreaseQuantity = () => {
        if (quantity <= 0) {
            Alert.alert(
                "Quantity",
                "Quantity cannot be less than 1",
                [
                    {text: "OK", onPress: () => setQuantity(1)}
                ],
            )
        }
        setQuantity(quantity - 1)
    }
    return (
        <View style={[styles.quantitySetter,layout.displayRow, layout.displayBetween, layout.alignCenter]}>
            {
                isCost &&
                <Text style={typography.costSubTitle}>{40 * quantity} Rs</Text>
            }
            <View style={[styles.quantityDetail, layout.displayRow, layout.displayBetween, layout.alignCenter]}>
                <TouchableOpacity
                    onPress={() => decreaseQuantity()}
                >
                    <Icon name='minus'
                          size={
                              vars.secondaryIconSize
                          }
                          color={colors.secondary}
                    />
                </TouchableOpacity>
                <Text style={typography.title}>{quantity}</Text>
                <TouchableOpacity
                    onPress={() => increaseQuantity()}
                >
                    <Icon name='plus'
                          size={
                              vars.secondaryIconSize
                          }
                          color={colors.dark}
                    />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create<QuantitySetterInterfaces>({
    quantitySetter:{
    },
    quantityDetail: {
        marginTop: 10,
        paddingHorizontal: 10,
        width: 100,
        borderRadius: 10,
        paddingVertical:5,
        backgroundColor: colors.softDark
    }
})

export default QuantitySetter;
