import {ViewStyle} from "react-native";

export interface QuantitySetterInterfaces {
    quantitySetter:ViewStyle
    quantityDetail:ViewStyle
}
