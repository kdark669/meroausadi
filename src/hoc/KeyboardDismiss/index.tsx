import React from 'react';
import {
    Keyboard,
    TouchableWithoutFeedback
} from 'react-native'

interface KeyboardDismissPropInterface {
    children:any
}

const KeyboardDismiss:React.FC<KeyboardDismissPropInterface> = (props) => {
    const {
        children
    } = props
    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} accessible={true}>
            {
                children
            }
        </TouchableWithoutFeedback>
    );
};

export default KeyboardDismiss;
