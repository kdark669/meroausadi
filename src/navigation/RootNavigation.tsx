import React from "react";
import {createStackNavigator} from '@react-navigation/stack';
import {RootNavigationParam} from "./interface/RootNavigationInterface";

import CustomerStack from "../modules/customer/navigation/CustomerStackNavigation";
import SupplierStack from "../modules/supplier/navigation/SupplierStackNavigation";
import AuthStack from "../modules/authentication/navigation/AuthenticationStack";

const Stack = createStackNavigator<RootNavigationParam>();

const RootNavigation: React.FC = () => {
    const isLoggedIn:boolean = true
    const role:string = 'customer'
    return (
        <Stack.Navigator
            screenOptions={{
                headerTransparent: true,
            }}
        >
            {
                isLoggedIn
                && (
                  <>
                      {
                          role === "supplier"
                          && <>
                              <Stack.Screen
                                  name="SupplierRoute"
                                  component={SupplierStack}
                                  options={{
                                      headerShown: false,
                                  }}
                              />
                          </>
                      }

                      {
                          role === "customer"
                          && <>
                              <Stack.Screen
                                  name="CustomerRoute"
                                  component={CustomerStack}
                                  options={{
                                      headerShown: false,
                                  }}
                              />
                          </>
                      }
                  </>
                )
            }
            {
                !isLoggedIn && <Stack.Screen
                    name="AuthStack"
                    component={AuthStack}
                    options={{
                        headerShown: false,
                    }}
                />
            }

        </Stack.Navigator>
    )
}

export default RootNavigation;
