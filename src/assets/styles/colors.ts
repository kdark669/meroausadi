export default {

    primary:'#3fc8f1',
    secondary:'#000B40',
    lightPrimary:'#91dff8',
    darkPrimary:'#1bbef1',
    default:'#ccc',
    danger:'#f16d6d',
    transparent:'transparent',

    //fonts colors
    light:'#f4f3f3',
    dark:'#1F1F1F',
    softDark:'#ccc',
    active:'#1bbef1'

}
