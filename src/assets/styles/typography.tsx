
import {StyleSheet, TextStyle, ViewStyle} from 'react-native';

import colors from './colors'
import vars from './vars'

interface Styles {
    title: TextStyle,
    subTitle: TextStyle,
    cardHeader:TextStyle
    costTitle:TextStyle
    costSubTitle:TextStyle
    linkText:TextStyle
    email:TextStyle
}

const typography = StyleSheet.create<Styles>({
    title: {
        fontSize: vars.title,
        fontWeight:"bold",
        textTransform:"capitalize"
    },
    subTitle:{
        fontSize: vars.subTitle,
        fontWeight:"600",
        textTransform:"capitalize"
    },
    costTitle:{
        fontSize: vars.costTitle,
        fontWeight:"600",
        color: colors.darkPrimary,
        textTransform:"capitalize"
    },
    costSubTitle:{
        fontSize: vars.costTitle,
        fontWeight:"600",
        color: colors.darkPrimary,
        textTransform:"capitalize"
    },

    cardHeader:{
        fontSize: 25,
        fontWeight:"400",
        color:colors.light
    },
    linkText:{
        color:colors.active
    },
    email:{
        fontSize: vars.costTitle,
        fontWeight:"600",
        color: colors.darkPrimary,
    }


})

export  default typography;
