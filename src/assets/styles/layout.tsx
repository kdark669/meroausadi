import {Platform, StyleSheet, TextStyle, ViewStyle} from 'react-native';

import colors from './colors'
import vars from './vars'
import {heightPercentageToDP as hp} from "react-native-responsive-screen";

interface Styles {
    flexOne?: ViewStyle
    flexTwo?: ViewStyle
    safeArea?: ViewStyle
    safeAreaContent?: ViewStyle
    safeAreaContentPadding?: ViewStyle
    displayRow?: ViewStyle
    displayCenter?: ViewStyle
    displayBetween?: ViewStyle
    displayEnd?: ViewStyle
    displayStart?: ViewStyle
    section?: ViewStyle
    boxShadow?: ViewStyle
    cardPadding?: ViewStyle
    containerPadding?: ViewStyle
    content?: ViewStyle
    inputStyle?: ViewStyle
    alignCenter?: ViewStyle
    textAlignCenter?: ViewStyle
    card?: ViewStyle
    activeTab?: ViewStyle
    headerDisplace?: ViewStyle
    img?: any
    rounderImg?: any
    circularImg?: any
    errorText: TextStyle
    inputBox: ViewStyle
    inputField: ViewStyle
    inputlLabel: TextStyle

}

const layout = StyleSheet.create<Styles>({

    flexOne: {
        flex: 1
    },

    flexTwo: {
        flex: 2
    },

    safeArea: {
        flex: 1,
    },

    safeAreaContent:{
        marginTop:Platform.OS === "ios" ? hp("4%"): hp("3%")
    },
    safeAreaContentPadding:{
        paddingHorizontal:20
    },
    displayRow: {
        flexDirection: "row"
    },

    displayCenter: {
        justifyContent: 'center',
    },

    displayBetween: {
        justifyContent: 'space-between',
    },

    displayEnd: {
        justifyContent: "flex-end"
    },
    displayStart: {
        justifyContent: "flex-start"
    },

    alignCenter: {
        alignContent: "center",
        alignItems: "center"
    },

    section: {
        marginTop: vars.sectionTop
    },
    headerDisplace:{
        marginTop: vars.contentTop
    },
    boxShadow: {
        shadowColor: colors.dark,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.1,
        shadowRadius: 2.62,
    },

    cardPadding: {
        padding: 10,
        flex: 1,
    },

    containerPadding: {
        padding: 20,
    },

    //card
    card: {
        flex: 1,
    },

    //images
    img: {
        width: "100%",
        height: "100%"
    },
    rounderImg: {
        width: "100%",
        height: "100%",
        borderRadius: 10,
        overflow: "hidden",
    },
    circularImg: {
        width: "100%",
        height: "100%",
        borderRadius: 100,
        overflow: "hidden"
    },

    //text
    textAlignCenter: {
        textAlign: "center"
    },
    activeTab: {
        backgroundColor: colors.primary,
        padding: 7,
        borderRadius: 100
    },
    errorText: {
        paddingVertical:5,
        color:colors.danger,
        textAlign:"center"
    },

    //form
    inputBox:{
        paddingVertical:hp("1%")
    },
    inputlLabel:{
        paddingVertical:5
    },
    inputField:{
        width:"100%",
        height:hp('5%'),
        backgroundColor:colors.softDark,
        borderRadius:vars.inputRadius,
        padding:Platform.OS === "ios" && vars.inputPadding || 4,
        paddingHorizontal:  vars.inputPadding,
    }

})

export default layout;
