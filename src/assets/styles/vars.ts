export default {

    //text
    title:20,
    subTitle:20,
    paragraph:5,
    costTitle:20,
    cardHeader:30,

    //borders
    cardRadius: 20,
    buttonRadius:10,
    inputRadius:7,

    //padding
    content:10,
    cardPadding:10,
    buttonPadding:10,
    buttonPaddingSecond: 5,
    sectionPadding:10,
    sectionTop:30,
    sectionBottom:10,
    contentTop:50,
    inputPadding:8,

    //icons
    iconSize:25,
    secondaryIconSize:15

}
