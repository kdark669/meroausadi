import React from 'react';
import {
    View,
    Text, StyleSheet, SafeAreaView, ScrollView, Platform
} from 'react-native'
import {HomePropsInterface, HomeStylesInterface} from "./interface/homeInterface";
import layout from "../../../../assets/styles/layout";
import HomeHead from "./components/homeHead";
import KeyboardDismiss from "../../../../hoc/KeyboardDismiss";
import CategorySlider from "./components/categorySlider";
import typography from "../../../../assets/styles/typography";
import MedSlider from "./components/medSlider";

const HomeScreen: React.FC<HomePropsInterface> = () => {
    return (
        <KeyboardDismiss>
            <>
                <HomeHead/>
                <SafeAreaView style={layout.safeArea}>
                    <ScrollView
                        keyboardDismissMode={"on-drag"}
                        keyboardShouldPersistTaps={"always"}
                    >
                        <View style={layout.safeAreaContent}>
                            <View style={[layout.safeAreaContentPadding]}>
                                <Text style={typography.subTitle}>
                                    Category
                                </Text>
                            </View>
                            <View>
                                <CategorySlider/>
                            </View>
                            <View style={[layout.safeAreaContent,layout.safeAreaContentPadding]}>
                                <Text style={typography.subTitle}>
                                    Common Medicine
                                </Text>
                            </View>
                            <View style={styles.medicineSlider}>
                                <MedSlider/>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        </KeyboardDismiss>
    );
};

const styles = StyleSheet.create<HomeStylesInterface>({
    medicineSlider:{
        flex:1,
        paddingTop:10,
        marginVertical: Platform.OS === "ios" ? 10 : 10
    }
})

export default HomeScreen
