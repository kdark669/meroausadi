import React from 'react';
import {Platform, StyleSheet, Text, View} from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";

import {HomeHeadPropsInterface, HomeHeadStylesInterface} from "../../interface/homeInterface";
import colors from "../../../../../../assets/styles/colors";
import Search from "../../../../../../common/partials/search";
import typography from "../../../../../../assets/styles/typography";
import layout from "../../../../../../assets/styles/layout";
const HomeHead:React.FC<HomeHeadPropsInterface> = (props) => {
    const {

    } = props
    return (
        <View style={styles.homeHeadContainer}>
            <View style={styles.homeHeaderSpacing}>
                <Text style={[typography.subTitle, layout.textAlignCenter]}>
                   Looking for your medicine
                </Text>
                <View style={[styles.searchArea]}>
                    <Search/>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create<HomeHeadStylesInterface>({
    homeHeadContainer:{
        backgroundColor: colors.primary,
        // height: Platform.OS === 'ios' ? hp('25%') : hp('25%'),
        height: hp('25%'),
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
    },
    homeHeaderSpacing:{
        marginTop:Platform.OS === "ios" ? hp('12%') : hp('8%'),
        marginHorizontal:wp('14%')
    },
    searchArea:{
        width:300,
        paddingVertical:20,
    }
})

export default HomeHead;
