import React from 'react';
import medList from "../../../../../../common/screens/med/mock/medList";
import MedItem from "../../../../../../common/screens/med/components/MedItem";
import MySlider from "../../../../../../common/partials/myslider";

const MedSlider = () => {
    return (
        <MySlider>
            {
                medList.map((med, index) => (
                    <MedItem item={med} key={index}/>
                ))
            }
        </MySlider>
    );
};

export default MedSlider;
