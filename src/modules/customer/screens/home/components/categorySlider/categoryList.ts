
let categoryList: {name:string, image:string }[];
categoryList = [
    {
        "name": "common",
        "image": "https://cdn.pixabay.com/photo/2012/04/10/17/40/vitamins-26622_1280.png",
    },
    {
        "name": "dental",
        "image": "https://www.pngkey.com/png/full/855-8555767_medicine-png-conventional-medicine-png.png",
    },
    {
        "name": "physical",
        "image": "https://cdn.mos.cms.futurecdn.net/uYgKBJ65z7eECQzL8tbiSS-320-80.jpg",
    },
    {
        "name": "cancer",
        "image": "https://tampabayscouting.org/wp/wp-content/uploads/2016/08/Pill-bottles.jpg",
    },
];

export default  categoryList
