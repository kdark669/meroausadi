import React from 'react';
import {StyleSheet, TouchableOpacity,Text, View,Image} from "react-native";
import {SliderItemPropsInterface, SliderItemStylesInterface} from "../interface/CategorySliderInterface";
import { useNavigation } from '@react-navigation/native';
import layout from "../../../../../../../assets/styles/layout";
import colors from "../../../../../../../assets/styles/colors";

const SliderItem:React.FC<SliderItemPropsInterface> = (props) => {
    const {
        category
    } = props
    const navigation = useNavigation();
    return (
        <TouchableOpacity
            onPress={() =>navigation.navigate('Category')}
        >
            <View style={[layout.displayBetween, layout.alignCenter, styles.categorySlider]}>
                <View style={styles.imageContainer}>
                    <Image
                        style={layout.circularImg}
                        resizeMode={'cover'}
                        source={{uri: category.image}}
                    />
                </View>
                <Text style={[layout.textAlignCenter,styles.categorySliderText]}>{category.name}</Text>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create<SliderItemStylesInterface>({
    imageContainer: {
        marginLeft: 15,
        height: 80,
        width: 80,
        borderRadius: 100,
        backgroundColor: colors.lightPrimary,
        aspectRatio:1,
        overflow: "hidden"
    },
    categorySlider: {
        marginTop: 10,
        marginRight: 10,
        textAlign:"center"
    },
    categorySliderText:{
        textAlign:"center",
        marginLeft:15
    }
})

export default SliderItem;
