import {TextStyle, ViewStyle} from "react-native";

export interface SliderItemPropsInterface {
    category: any
}

export interface SliderItemStylesInterface {
    imageContainer: ViewStyle
    categorySlider: ViewStyle
    categorySliderText: TextStyle
}
