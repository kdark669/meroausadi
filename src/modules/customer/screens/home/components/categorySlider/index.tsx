import React from 'react';
import { View} from "react-native";
import MySlider from "../../../../../../common/partials/myslider";
import categoryList from "./categoryList";
import SliderItem from "./containers/SliderItem";

const CategorySlider = () => {
    return (
        <View>
            <MySlider>
                {
                    categoryList.map(
                        (category, index) => (
                            <SliderItem category={category} key={index}/>
                        ))
                }
            </MySlider>
        </View>
    );
};

export default CategorySlider;