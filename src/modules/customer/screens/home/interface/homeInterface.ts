import {ViewStyle} from "react-native";

export interface HomePropsInterface {

}

export interface HomeHeadPropsInterface {

}

export interface MedDetailHeaderPropsInterface {

}

export interface HomeStylesInterface {
    medicineSlider: ViewStyle
}

export interface HomeHeadStylesInterface {
    homeHeadContainer: ViewStyle
    homeHeaderSpacing: ViewStyle
    searchArea: ViewStyle
}

export interface MedDetailHeaderStylesInterface {
    homeHeadContainer: ViewStyle
    homeHeaderSpacing: ViewStyle
    imageContainer: any

}
