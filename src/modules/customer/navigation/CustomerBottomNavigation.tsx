import React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {CustomerBottomNavigationInterface} from "./interface/CustomerNavigationInterface";

import colors from "../../../assets/styles/colors";
import vars from "../../../assets/styles/vars";

import HomeScreen from "../screens/home";
import MyIcon from "../../../common/partials/myicon";


const Tab = createMaterialBottomTabNavigator<CustomerBottomNavigationInterface>();

const CustomerBottomNavigation = () => {
    const ACTIVE_TAB_COLOR = colors.active
    const INACTIVE_TAB_COLOR = colors.dark
    return (
        <>
            <Tab.Navigator
                initialRouteName="Home"
                shifting={true}
                labeled={false}
                activeColor={ACTIVE_TAB_COLOR}
                inactiveColor={INACTIVE_TAB_COLOR}
                barStyle={{
                    position: 'absolute',
                    shadowColor: colors.dark,
                    backgroundColor: colors.light
                }}
            >
                <Tab.Screen
                    name="Home"
                    component={HomeScreen}
                    options={{
                        tabBarLabel: 'Home',
                        tabBarIcon: ({ focused }) =>
                            <MyIcon
                                iconName={"home"}
                                iconColor={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR}
                                iconSize={vars.iconSize}
                            />
                    }}
                />
                <Tab.Screen
                    name="Category"
                    component={HomeScreen}
                    options={{
                        tabBarLabel: 'Home',
                        tabBarIcon: ({ focused }) =>
                            <MyIcon
                                iconName={"list"}
                                iconColor={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR}
                                iconSize={vars.iconSize}
                            />
                    }}
                />

            </Tab.Navigator>
        </>
    );
};

export default CustomerBottomNavigation;
