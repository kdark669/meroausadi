import React from "react";
import {createStackNavigator} from '@react-navigation/stack';
import { CustomerStackNavigationInterface } from "./interface/CustomerNavigationInterface";

import HeaderRight from "../../../common/partials/header/screen/HeaderRight";
import HeaderLeft from "../../../common/partials/header/screen/HeaderLeft";

import CustomerBottomNavigation from "./CustomerBottomNavigation";
import MedDetail from "../../../common/screens/med/components/medDetail";
import Cart from "../screens/cart";

const Stack = createStackNavigator<CustomerStackNavigationInterface>();

const CustomerStack: React.FC = () => {
    return (
        <Stack.Navigator
            initialRouteName="CustomerLayout"
            screenOptions={{
                headerTransparent: true,
            }}
        >
            <Stack.Screen
                name="CustomerLayout"
                component={CustomerBottomNavigation}
                options={{
                    headerShown: true,
                    title: "",
                    headerRight: () => <HeaderRight/>,
                    headerLeft:() => <HeaderLeft/>
                }}
            />
            <Stack.Screen
                name="MedDetail"
                component={MedDetail}
                options={{
                    headerShown: true,
                    title: "",
                    headerRight: () => <HeaderRight/>
                }}
            />
            <Stack.Screen
                name="Cart"
                component={Cart}
                options={{
                    headerShown: true,
                    title: "",
                    headerRight: () => <HeaderRight/>
                }}
            />
        </Stack.Navigator>
    )
}

export default CustomerStack;
