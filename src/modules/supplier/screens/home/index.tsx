import React from 'react';
import {
    View,
    Text, StyleSheet, SafeAreaView
} from 'react-native'
import {SupplierHomePropsInterface, SupplierHomeStylesInterface} from "./interface/supplierHomeInterface";
import layout from "../../../../assets/styles/layout";

const SupplierHome:React.FC<SupplierHomePropsInterface> = () => {
    return (
        <SafeAreaView style={layout.safeArea}>
            <View style={layout.safeAreaContent}>
                <Text> I am supplier home screen </Text>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create<SupplierHomeStylesInterface>({

})

export default SupplierHome
