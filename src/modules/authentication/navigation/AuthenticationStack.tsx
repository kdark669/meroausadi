import React from "react";
import {createStackNavigator} from '@react-navigation/stack';
import { AuthenticationStackInterface } from "./interface/AuthenticationStackInterface";

import SignIn from "../screens/signin";

const Stack = createStackNavigator<AuthenticationStackInterface>();

const AuthStack: React.FC = () => {
    return (
        <Stack.Navigator
            initialRouteName="SignIn"
            screenOptions={{
                headerTransparent: true,
            }}
        >
            <Stack.Screen
                name="SignIn"
                component={SignIn}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    )
}

export default AuthStack;
